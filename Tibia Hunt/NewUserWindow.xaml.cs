﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tibia_Hunt.Classes;

namespace Tibia_Hunt
{
    /// <summary>
    /// Interaction logic for NewUserWindow.xaml
    /// </summary>
    public partial class NewUserWindow : Window
    {
        List<CharacterClass> characterClasses = new List<CharacterClass>();
        public NewUserWindow()
        {
            InitializeComponent();

            characterClasses.Add(new CharacterClass("Druid", "Images/icon_druid.png"));
            characterClasses.Add(new CharacterClass("Knight", "Images/icon_knight.png"));
            characterClasses.Add(new CharacterClass("Mage", "Images/icon_mage.png"));
            characterClasses.Add(new CharacterClass("Paladin", "Images/icon_Paladin.png"));

            _cmbCharacterClass.ItemsSource = characterClasses;

        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {

            CharacterClass characterClass = (CharacterClass)_cmbCharacterClass.SelectedValue;
            UserCharacter userCharacter = new UserCharacter(_textBoxCharacter.Text, characterClass.Name);

            using (SQLiteConnection connection = new SQLiteConnection(App.databasePath))
            {
                connection.CreateTable<UserCharacter>();
                connection.Insert(userCharacter);
            }


            Close();
        }

        private void _cmbClass_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CharacterClass testvar = (CharacterClass)_cmbCharacterClass.SelectedValue;
            testLabel.Content = testvar.Name;
            

        }
    }
}
