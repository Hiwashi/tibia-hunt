﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Tibia_Hunt
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        static string databaseName = "TibiaHuntCharacters.db";
        // TODO: Make this the app folder instead of My Documents.
        static string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); 
        public static string databasePath = System.IO.Path.Combine(folderPath, databaseName);
    }
}
