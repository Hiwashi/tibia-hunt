﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tibia_Hunt.Classes
{
    public class UserCharacter
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }

        public UserCharacter()
        {

        }
        public UserCharacter(string name, string @class)
        {
            Name = name;
            Class = @class;
        }

    }
}
