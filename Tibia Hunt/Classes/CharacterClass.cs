﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tibia_Hunt.Classes
{
    public class CharacterClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }

        public CharacterClass(string name, string imagePath)
        {
            Name = name;
            ImagePath = imagePath;
        }
    }
}
