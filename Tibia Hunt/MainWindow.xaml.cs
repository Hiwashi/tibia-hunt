﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tibia_Hunt.Classes;

namespace Tibia_Hunt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ReadDatabase();
        }

        void ReadDatabase()
        {
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.databasePath))
            {
                connection.CreateTable<UserCharacter>();
                List<UserCharacter> userCharacter = connection.Table<UserCharacter>().ToList();
                _listViewCharacters.ItemsSource = userCharacter;
            }
        }

        private void CharacterAdd_Click(object sender, RoutedEventArgs e)
        {
            NewUserWindow newUserWindow = new NewUserWindow();
            newUserWindow.ShowDialog();
            ReadDatabase();
        }

        private void CharacterEdit_Click(object sender, RoutedEventArgs e)
        {

        }
        private void CharacterRemove_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
